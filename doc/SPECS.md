# Projet Ortho pro

## Description du besoin
Le programme *Ortho pro* devra permettre à l'utilisateur de :

* Identifier son profil orthographique
* Repérer des erreurs d'orthographe
* Assimiler les régularités orthographique
* Ecrire une phrase sous la dictée sans fautes d'orthographe

Pour cela le programme *Ortho pro* devra :

* Permettre à l'utilisateur de lancer une dictée au format audio et taper du texte pendant et après la lecture de celle-çi.
* Analiser et mémoriser (pour chaque utilisateurs) le type de fautes d'orthographe et leurs fréquence.
* 

Faire un programme qui prend une dictée en audio et demande à l'utilisateur de taper la dictée.

Une fois que l'utilisateur aura valider son entrée, le programme devra analyser ce que l'utilisateur aura taper pour y déceller des fautes d'orthographes.

Pour chaques phrases qui contient une ou plusieurs fautes d'orthographes, le programme devra selectioner un type de faute et donner un conseil associé.

Ex : une faute sur a/à donnera l'indice "avait ou non ?" en surlignant la/les fautes associées

Un seul indice est donné par phrase.

## Solution proposée

