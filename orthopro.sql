--
-- Base de données: `orthopro`
--

-- --------------------------------------------------------

--
-- Structure de la table `dictee`
--

  
CREATE TABLE IF NOT EXISTS dictee (
  idDictee integer NOT NULL PRIMARY KEY AUTOINCREMENT,
  idUser integer NOT NULL,
  idPhrase integer NOT NULL,
  dateDictee datetime default current_timestamp,
  --Permet de connaittre le niveau de l'utilisateur par rapport à un type donné
  resultat integer default 0,
  PRIMARY KEY (idUser, idPhrase),
  FOREIGN KEY (idUser) REFERENCES utilisateur(idUser) ON DELETE CASCADE,
  FOREIGN KEY (idPhrase) REFERENCES phrase(idPhrase) ON DELETE CASCADE
);

-- --------------------------------------------------------

--
-- Structure de la table `phrase`
--

CREATE TABLE phrase (
  idPhrase integer PRIMARY KEY AUTOINCREMENT,
  textePhrase varchar(256) NOT NULL,
  indiceCorrection varchar(256) NOT NULL,
  regleCorrection varchar(256) NOT NULL,
  idType int(11) NOT NULL,
  FOREIGN KEY (idType) REFERENCES typedifficulte(idType) ON UPDATE CASCADE, 
  FOREIGN KEY (idType) REFERENCES typedifficulte(idType) ON DELETE CASCADE
);

-- --------------------------------------------------------

--
-- Structure de la table `typedifficulte`
--

CREATE TABLE IF NOT EXISTS typedifficulte (
  idType integer NOT NULL PRIMARY KEY AUTOINCREMENT,
  description varchar(256) NOT NULL
);

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE IF NOT EXISTS utilisateur (
  idUser integer NOT NULL PRIMARY KEY AUTOINCREMENT,
  firstName varchar(256) NOT NULL,
  lastName varchar(256) NOT NULL,
  login varchar(256) NOT NULL UNIQUE,
  Password varchar(256) NOT NULL,
 --Permet de savoir si un utilisateur a droit d'accéder à certaines parties de l'application.Comme par exemple 
 --visualiser la liste des Users
 --Enregistrer des exercies et des types d'exercices ou les modifier.
  categorie varchar(256) NOT NULL,
  dateInscription datetime default current_timestamp,
  derniereConnexion datetime default current_timestamp
);
