package source.phrase;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import source.Auditor;
import source.type.Type;
import source.type.TypeService;

@Controller
@RequestMapping(value ="/sentences")
public class SentenceController{

	@Resource
	private TypeService typeService;
	
	@Resource
	private SentenceService phraseService;
	
	@RequestMapping(value ="/getAllPhrases",method = RequestMethod.GET)
	public @ResponseBody Collection<Sentence> getAllPhrase(){
		return phraseService.getAllPhrase();
	} 
	
	@RequestMapping(value ="/getPhraseById",method = RequestMethod.GET)
	public @ResponseBody Sentence getPhraseById(long id){
		return phraseService.getPhraseById(id);
	} 
	
	@RequestMapping(value ="/getSentenceByType",method = RequestMethod.GET)
	public @ResponseBody List<Sentence> getSentenceByType(HttpSession session){
		return phraseService.getSentenceByType((String) session.getAttribute("phraseParType"));
	}
	
	@RequestMapping(value ="/findPhraseById",method = RequestMethod.GET)
	public @ResponseBody Sentence findPhraseById(HttpSession session){
		return (Sentence) session.getAttribute("phrase");
	} 
	
	@RequestMapping(value ="/insertSentence",method = RequestMethod.POST)
	public @ResponseBody void createPhrase(@RequestBody Map<String, String> phrase/*@ModelAttribute @Valid Sentence phrase,BindingResult bindingResult,Model model*/){
		Type type = typeService.getTypeById(Long.valueOf(phrase.get("type")));
		
		Sentence sentence = new Sentence();
		sentence.setTextePhrase(phrase.get("textePhrase"));
		sentence.setIndiceCorrection(phrase.get("indiceCorrection"));
		sentence.setRegleCorrection(phrase.get("regleCorrection"));
		sentence.setType(type);
		Auditor.writeInExcelFile(sentence);
		phraseService.createPhrase(sentence);
	}
	
	@RequestMapping(value ="/insertSentence",method = RequestMethod.GET)
	public String createPhrase(Model model){
		model.addAttribute("formSentence", new Sentence());
		return "formSentence";
	}
	
	@RequestMapping(value ="/play/{id}",method = RequestMethod.GET)
	public @ResponseBody void playAudio(@PathVariable(value="id") long idSentence){
		String texte = getPhraseById(idSentence).getTextePhrase();
		phraseService.play(texte);
	}
	
	@RequestMapping(value ="/doExercise",method = RequestMethod.GET)
	public String listeExercice(){
		return "listeExercice";
	}
	
	@RequestMapping(value ="/sentencesByType/{id}",method = RequestMethod.GET)
	public String sentencesByType(@PathVariable(value="id") String description,HttpSession session){
		Objects.requireNonNull(session);
		session.setAttribute("phraseParType", description);
		return "dicteeParType";
	}
	
	@RequestMapping(value ="/searchSentenceByType",method = RequestMethod.GET)
	public @ResponseBody List<Sentence> searchSentenceByType(HttpSession session){
		return phraseService.searchSentence((String) session.getAttribute("phraseParType"));
	}
	
	@RequestMapping(value ="/searchSentence",method = RequestMethod.GET)
	public String searchSentence(){
		return "search";
	}
}
