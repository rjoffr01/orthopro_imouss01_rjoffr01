package source.phrase;

import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import source.Auditor;
import source.type.Type;

@Entity
@Table(name="phrase")
public class Sentence {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY )
	private long idPhrase;
	private String textePhrase;
	private String indiceCorrection;
	private String regleCorrection;
    @ManyToOne( cascade = {CascadeType.REMOVE} )
	@JoinColumn(name = "idType")
	private Type type;
    @Transient
	private final int hashCode = 1000;

	public final long getIdPhrase() {
		return idPhrase;
	}
	public final void setIdPhrase(long idPhrase) {
		this.idPhrase = idPhrase;
	}
	public final String getTextePhrase() {
		return textePhrase;
	}
	public final void setTextePhrase(String textePhrase) {
		Objects.requireNonNull(textePhrase);
		Auditor.isEmpty(textePhrase);
		this.textePhrase = textePhrase;
	}
	public final String getIndiceCorrection() {
		return indiceCorrection;
	}
	public final void setIndiceCorrection(String indiceCorrection) {
		Objects.requireNonNull(indiceCorrection);
		Auditor.isEmpty(indiceCorrection);
		this.indiceCorrection = indiceCorrection;
	}
	public final String getRegleCorrection() {
		return regleCorrection;
	}
	public final void setRegleCorrection(String regleCorrection) {
		Objects.requireNonNull(regleCorrection);
		Auditor.isEmpty(regleCorrection);
		this.regleCorrection = regleCorrection;
	}
	public final Type getType() {
		return type;
	}
	public final void setType(Type type) {
		Objects.requireNonNull(type);
		this.type = type;
	}
	
	@Override
	public boolean equals(Object o) {
		if(!(o instanceof Sentence)) return false;

		Sentence s = (Sentence) o;
		
		if(!textePhrase.equalsIgnoreCase(s.getTextePhrase())) return false;
		
		if(!indiceCorrection.equalsIgnoreCase(s.getIndiceCorrection())) return false;
		
		if(!regleCorrection.equalsIgnoreCase(s.getRegleCorrection())) return false;
		
		if(!type.equals(s.type)) return false;
		
		return true;
	}
	
	@Override
	public int hashCode() {
		return hashCode;
	}
}
