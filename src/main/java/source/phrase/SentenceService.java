package source.phrase;

import java.util.Collection;
import java.util.List;

public interface SentenceService {

	Collection<Sentence> getAllPhrase();
	Sentence getPhraseById(long idPrase);
	void createPhrase(Sentence phrase);
	void updatePhrase(Sentence phrase,long idPhrase);
	void deletPhrase(long idPhrase);
	List<Sentence> getSentenceByType(String description);
	List<Sentence> searchSentence(String description);
	void play(String texte);
}
