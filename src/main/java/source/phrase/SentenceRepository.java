package source.phrase;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface SentenceRepository extends CrudRepository<Sentence, Long>{
	@Query("SELECT s FROM Sentence s WHERE s.type.description = :description")
	List<Sentence> getSentenceByType(@Param("description") String description);
	@Query("SELECT s FROM Sentence s WHERE s.type.description LIKE %:description%")
	List<Sentence> searchSentence(@Param("description") String description);
}
