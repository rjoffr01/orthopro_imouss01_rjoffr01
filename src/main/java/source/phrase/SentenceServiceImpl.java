package source.phrase;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections4.IteratorUtils;
import org.springframework.stereotype.Service;

import t2s.son.LecteurTexte;


@Service
public class SentenceServiceImpl implements SentenceService{
	
	@Resource
	private SentenceRepository phraseRepository;
	
	@Override
	public Collection<Sentence> getAllPhrase(){
		return Collections.unmodifiableCollection(IteratorUtils.toList(phraseRepository.findAll().iterator()));
	}

	@Override
	public Sentence getPhraseById(long idPrase) {
		return phraseRepository.findOne(idPrase);
	}
	
	@Override
	public List<Sentence> getSentenceByType(String description) {
		return phraseRepository.getSentenceByType(description);
	}

	@Override
	public void createPhrase(Sentence phrase) {
		//phraseRepository.save(Auditor.createSentence());
		phraseRepository.save(phrase);
	}

	@Override
	public void updatePhrase(Sentence phrase, long idPhrase) {
		phraseRepository.save(phrase);
	}

	@Override
	public void deletPhrase(long idPhrase) {
		phraseRepository.delete(idPhrase);
	}

	@Override
	public void play(String texte) {
		LecteurTexte lecteur = new LecteurTexte(texte);
		lecteur.playAll();
	}

	@Override
	public List<Sentence> searchSentence(String description) {
		return phraseRepository.searchSentence(description);
	}

}
