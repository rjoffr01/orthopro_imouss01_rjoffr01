package source.type;

import org.springframework.data.repository.CrudRepository;

public interface TypeRepository extends CrudRepository<Type,Long> {
	Type findByDescription(String description);
}
