package source.type;

import java.util.Collection;
import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(value ="/types")
public class TypeController {

	@Resource
	private TypeService typeService;
	
	@RequestMapping(value= "/getAllTypes", method = RequestMethod.GET)
	public @ResponseBody Collection<Type> getAllTypes() {
		return this.typeService.getAllTypes();
	}
	
	@RequestMapping(value = "/getTypeByID/{id}",method = RequestMethod.GET)
	public String getTypeById(@PathVariable(value = "id") long id,Model model) {
		model.addAttribute("updateType", typeService.getTypeById(id));
		return "updateType";
	}
	
	@RequestMapping(value = "/description/{description}",method = RequestMethod.GET)
	public@ResponseBody Type getTypeByDescription(@PathVariable(value = "description") String description) {
		return this.typeService.getTypeByDescription(description);
	}
	
	@RequestMapping(value = "/insert",method = RequestMethod.POST, consumes="application/json")
	public @ResponseBody void createType(@RequestBody Type type) {
		typeService.createType(type);
	}

	@RequestMapping(value = "/insert",method = RequestMethod.GET)
    public String createTypeForm() {
        return "insertType";
    }

	@RequestMapping(value = "/updateType",method = RequestMethod.POST)
	public @ResponseBody void updateType(@RequestBody Type type) {
		type.setIdType(type.getIdType());
		typeService.updateType(type);
	}

	@RequestMapping(value = "/deleteType/{id}",method = RequestMethod.GET)
	public @ResponseBody void deleteType(@PathVariable(value = "id") long id) {
		this.typeService.deleteType(id);
	}
	
	@RequestMapping(value = "/listeType",method = RequestMethod.GET)
    public String getListeType() {
        return "listeType";
    }
}
