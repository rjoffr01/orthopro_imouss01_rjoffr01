package source.type;

import java.util.Collection;

public interface TypeService {
	Collection<Type>  getAllTypes();
	Type getTypeById(Long id);
	void createType(Type type);
	void updateType(Type type);
	Type getTypeByDescription(String description);
	void deleteType(Long id);
}
