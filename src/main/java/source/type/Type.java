package source.type;

import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import source.Auditor;

@Entity
@Table(name="typedifficulte")
public class Type {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY )
	private long idType;
	private String description;
	@Transient
	private final int hashCode = 10000;
	
	public final String getDescription() {
		return description;
	}
	
	public final  void setDescription(String description) {
		Objects.requireNonNull(description);
		Auditor.isEmpty(description);
		this.description = description;
	}
	
	public final long getIdType() {
		return idType;
	}
	
	public final void setIdType(long idType) {
		Auditor.requireNonNegativeValue(idType);
		this.idType = idType;
	}
	
	@Override
	public boolean equals(Object o) {
		if(!(o instanceof Type)) return false;

		Type t = (Type) o;
		
		return description.equals(t.description);
	}
	
	@Override
	public int hashCode() {
		return hashCode;
	}
}
