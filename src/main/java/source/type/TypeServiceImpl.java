package source.type;

import java.util.Collection;
import java.util.Collections;
import java.util.Objects;

import javax.annotation.Resource;
import org.apache.commons.collections4.IteratorUtils;
import org.springframework.stereotype.Service;

import source.Auditor;

@Service(value="typeService")
public class TypeServiceImpl implements TypeService {

	@Resource
	private TypeRepository typeRepository;
	
	@Override
	public Collection<Type> getAllTypes() {
		return Collections.unmodifiableCollection(IteratorUtils.toList(this.typeRepository.findAll().iterator()));
	}

	@Override
	public Type getTypeById(Long id) {
		Auditor.requireNonNegativeValue(id);
		return this.typeRepository.findOne(id);
	}

	@Override
	public Type getTypeByDescription(String description) {
		Objects.requireNonNull(description);
		Auditor.isEmpty(description);
		return this.typeRepository.findByDescription(description);
	}
	
	@Override
	public void createType(Type type) {
		Objects.requireNonNull(type);
		this.typeRepository.save(type);
		//this.typeRepository.save(Auditor.createType());
	}

	@Override
	public void updateType(Type type) {
		Objects.requireNonNull(type);
		this.typeRepository.save(type);
	}

	@Override
	public void deleteType(Long id) {
		Auditor.requireNonNegativeValue(id);
		this.typeRepository.delete(id);
	}
}
