package source;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import source.phrase.Sentence;
import source.type.Type;

public final class Auditor {

	private final static String path = "src/main/resources/orthopro.xlsx";

	private Auditor() {
		/*
		 * this class is not to be instantiated. It just allows to declare static
		 * methods allowing to control the values
		 */
	}

	public static void requireNonNegativeValue(long value) {
		if (value <= 0) {
			throw new IllegalArgumentException("ID can't be negative.");
		}
	}

	public static void isEmpty(String description) {
		if (description.isEmpty()) {
			throw new IllegalArgumentException("Can't accept empty value.");
		}
	}

	private static final Set<XSSFRow> readExcelFile() {
		HashSet<XSSFRow> set = new HashSet<>();
		try(	final InputStream in = Files.newInputStream(Paths.get(path));
				final XSSFWorkbook workbook = new XSSFWorkbook(in)
			){
			final XSSFSheet sheet = workbook.getSheet("orthopro");
			int i = 1;
			XSSFRow row = sheet.getRow(i);
			while (row != null) {
				set.add(row);
				row = sheet.getRow(i++);
			}
		} catch (IOException e) {
			throw new IllegalStateException("File or sheet doesn't exist.");
		}
		return set;
	}


	public static final void writeInExcelFile(Sentence sentence){
		Objects.requireNonNull(sentence);
		
		try(	final InputStream in = Files.newInputStream(Paths.get(path));
				final XSSFWorkbook workBook = new XSSFWorkbook(in);
				final OutputStream out = Files.newOutputStream(Paths.get(path));
			){
			
			XSSFSheet sheet = workBook.getSheet("orthopro");
			int lastRowNumber = sheet.getLastRowNum();
			XSSFRow row = sheet.createRow(++lastRowNumber);
			row.createCell(0).setCellValue(sentence.getTextePhrase());
			row.createCell(1).setCellValue(sentence.getIndiceCorrection());
			row.createCell(2).setCellValue(sentence.getRegleCorrection());
			row.createCell(3).setCellValue(sentence.getType().getDescription());			
			
			workBook.write(out);
		}catch (EncryptedDocumentException | IOException e) {
			throw new IllegalStateException("File or sheet doesn't exist.");
		}
	}

	public static Set<Sentence> createSentence(){
		return readExcelFile().stream().filter(row -> !row.getCell(0).getStringCellValue().isEmpty()
				&& !row.getCell(1).getStringCellValue().isEmpty() && !row.getCell(2).getStringCellValue().isEmpty()
				&& !row.getCell(3).getStringCellValue().isEmpty()).map(row -> {
					Sentence sentence = new Sentence();
					sentence.setTextePhrase(row.getCell(0).getStringCellValue().trim());
					sentence.setIndiceCorrection(row.getCell(1).getStringCellValue().trim());
					sentence.setRegleCorrection(row.getCell(2).getStringCellValue().trim());
					Type type = new Type();
					type.setDescription(row.getCell(3).getStringCellValue().trim());
					sentence.setType(type);
					return sentence;
				}).collect(Collectors.toSet());
	}

	public static Set<Type> createType(){
		return readExcelFile().stream().filter(row -> !row.getCell(3).getStringCellValue().isEmpty()).map(row -> {
			Type type = new Type();
			type.setDescription(row.getCell(3).getStringCellValue());
			return type;
		}).collect(Collectors.toSet());
	}
}
