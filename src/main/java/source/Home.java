package source;


import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Objects;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import source.user.User;
import source.user.UserServiceImpl;
import t2s.son.LecteurTexte;

@Controller
@RequestMapping(value ="/orthopro")
public class Home {
	
	@Resource
	private UserServiceImpl userServiceImpl;
	
	@RequestMapping(value = "/home",method = RequestMethod.GET)
    public String accueil() {
        return "home";
    }
	
	@RequestMapping(value = "/login",method = RequestMethod.GET)
    public String login() {
        return "login";
    }
	
	@RequestMapping(value = "/login",method = RequestMethod.POST)
    public @ResponseBody void login(@RequestBody User user,HttpSession session) {
		Objects.requireNonNull(user);		
		User u = userServiceImpl.getUserByLoginAndPassword(user.getLogin(), user.getPassword());
		if(u != null) {
			LecteurTexte lecteur = new LecteurTexte("Bienvenue " + u.getLastName() + " " + u.getFirstName() + "." + 
					"Vous �tes au bon endroit si vous souhaitez vous perfectionnez en jargon genie civil");
			lecteur.playAll();
		}else {
			LecteurTexte lecteur = new LecteurTexte("Identifiant incorrect.Essayer en tapant les bonnes informations.");
			lecteur.playAll();
		}
		session.setAttribute("user", u);
    }
	
	/***@RequestMapping(value = "/checkConnection",method = RequestMethod.GET)
    public String checkConnection(HttpSession session) {
		User user = (User) session.getAttribute("user");
		if(user == null) return "login";
        return "home";
    }*/
	
	@RequestMapping(value = "/logout",method = RequestMethod.GET)
    public String logout(HttpSession session) {
		Objects.requireNonNull(session);		
		User user = (User) session.getAttribute("user");
		String derniereConnexion = LocalDateTime.now().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME).toString()
				.replace("T", " ").substring(0, 19);
		user.setDerniereConnexion(derniereConnexion);
		userServiceImpl.updateUser(user, session);
		session.invalidate();
		return "login";
	}
}
