package source.dictee;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.Objects;

import javax.annotation.Resource;

import org.apache.commons.collections4.IteratorUtils;
import org.springframework.stereotype.Service;

import source.Auditor;
import source.phrase.Sentence;
import source.phrase.SentenceService;
import source.user.User;
import source.user.UserService;

@Service(value="dicteeService")
public class DicteeServiceImpl implements DicteeService {

	@Resource
	private DicteeRepository dicteeRepository;
	@Resource
	private UserService userService;
	@Resource
	private SentenceService phraseService;
	
	@Override
	public void createDictee(Map<String, String> dicte) {
		Objects.requireNonNull(dicte);
		Dictee dictee = new Dictee();
		User user = userService.getUserById(Long.valueOf(dicte.get("user")));
		Sentence sentence = phraseService.getPhraseById(Long.valueOf(dicte.get("sentence")));
		
		String dateDictee = LocalDateTime.now().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME).toString()
				.replace("T", " ").substring(0, 19);
		
		dictee.setUser(user);
		dictee.setSentence(sentence);
		dictee.setDateDictee(dateDictee);
		if(sentence.getRegleCorrection().trim().equals(dicte.get("reponse").trim())) dictee.setResultat(1);
		else dictee.setResultat(0);
		dicteeRepository.save(dictee);
	}

	@Override
	public void deleteDictee(long idDictee) {
		Auditor.requireNonNegativeValue(idDictee);
		dicteeRepository.delete(idDictee);
	}

	/*@Override
	public Dictee getDicteeById(long idDictee) {
		Auditor.requireNonNegativeValue(idDictee);
		return dicteeRepository.findOne(idDictee);
	}*/

	@Override
	public Collection<Dictee> getAllDictee() {
		return  Collections.unmodifiableCollection(IteratorUtils.toList(dicteeRepository.findAll().iterator()));
	}
}
