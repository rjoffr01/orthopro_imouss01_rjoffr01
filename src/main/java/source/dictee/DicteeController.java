package source.dictee;

import java.util.Collection;
import java.util.Map;
import java.util.Objects;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import source.phrase.SentenceService;
import source.user.UserService;

@Controller
@RequestMapping(value="/dictees")
public class DicteeController {

	@Resource
	private DicteeService dicteeService;
	@Resource
	private UserService userService;
	@Resource
	private SentenceService phraseService;
	
	@RequestMapping(value="/insertDictee/{id}",method = RequestMethod.GET)
	public String createDictee(@PathVariable long id,HttpSession session){
		Objects.requireNonNull(session);
		session.setAttribute("phrase", phraseService.getPhraseById(id));
		return "dictee";
	}
	
	@RequestMapping(value="/insertDictee",method = RequestMethod.POST,consumes = "application/json")
	public @ResponseBody void createDictee(@RequestBody Map<String, String> dicte){		
		dicteeService.createDictee(dicte);
	}
	
	@RequestMapping(value="/deleteDictee",method = RequestMethod.GET)
	public @ResponseBody void deleteDictee(long idDictee){
		dicteeService.deleteDictee(idDictee);
	}
	
	@RequestMapping(value="/getAllDictee",method = RequestMethod.GET)
	public Collection<Dictee> getAllDictee(){
		return dicteeService.getAllDictee();
	}
}
