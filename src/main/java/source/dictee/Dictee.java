package source.dictee;

import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import source.phrase.Sentence;
import source.user.User;

@Entity
@Table(name="dictee")
public class Dictee {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY )
	private long idDictee;
	
	@JoinColumn(name = "idUser")
	@ManyToOne(cascade = {CascadeType.REMOVE})
	private User user;

	@JoinColumn(name = "idPhrase")
	@ManyToOne(cascade = {CascadeType.REMOVE})
	private Sentence sentence;
	
	private int resultat;
	private String dateDictee;


	public final User getUser() {
		return user;
	}

	public final void setUser(User user) {
		Objects.requireNonNull(user);
		this.user = user;
	}
	
	public final Sentence getSentence() {
		return sentence;
	}

	public final void setSentence(Sentence sentence) {
		Objects.requireNonNull(sentence);
		this.sentence = sentence;
	}

	public final int getResultat() {
		return resultat;
	}

	public final void setResultat(int resultat) {
		if(resultat < 0) {
			throw new IllegalArgumentException("ID can't be negative.");
		}
		this.resultat = resultat;
	}

	public final long getId() {
		return idDictee;
	}

	public final void setId(long idDictee) {
		this.idDictee = idDictee;
	}

	public final String getDateDictee() {
		return dateDictee;
	}

	public final void setDateDictee(String dateDictee) {
		this.dateDictee = dateDictee;
	}
}
