package source.dictee;

import java.util.Collection;
import java.util.Map;

public interface DicteeService {
	void createDictee(Map<String, String> dicte);
	void deleteDictee(long idDictee);
	Collection<Dictee> getAllDictee();
}