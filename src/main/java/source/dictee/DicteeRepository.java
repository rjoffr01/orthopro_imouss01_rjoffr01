package source.dictee;

import org.springframework.data.repository.CrudRepository;


public interface DicteeRepository extends CrudRepository<Dictee,Long>{

}
