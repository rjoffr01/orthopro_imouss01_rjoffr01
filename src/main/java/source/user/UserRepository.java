package source.user;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import source.dictee.Dictee;

public interface UserRepository extends CrudRepository<User, Long>{
	@Query("SELECT u FROM User u WHERE u.login = :login and u.password = :password")
	User getUserByLoginAndPassword(@Param("login") String login,@Param("password") String password);
	@Query("SELECT d FROM Dictee d WHERE d.user.idUser = :userID")
	List<Dictee> getDicteeByUser(@Param("userID") long idUse);
}
