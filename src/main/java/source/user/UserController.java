package source.user;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import source.dictee.Dictee;
import source.dictee.DicteeService;

@Controller
@RequestMapping(value="/users")
public class UserController {

	@Resource
	private UserService userService;
	@Resource
	private DicteeService dicteeService;
	@Resource
	private UserServiceImpl userServiceImpl;
	
	@RequestMapping(value="/getUserById", method = RequestMethod.GET)
	public @ResponseBody User getUserById(HttpSession session) {
		Objects.requireNonNull(session);
		User user = (User) session.getAttribute("user");
		if(user == null) throw new IllegalStateException("Utilisateur non connecte...");
		long idUser = user.getIdUser();
		return userService.getUserById(idUser);
	}
	
	@RequestMapping(value="/insertUser", method = RequestMethod.POST,consumes="application/json")
	public @ResponseBody void createUser(@RequestBody User user) {
		userService.createUser(user);
	}

	@RequestMapping(value="/insertUser", method = RequestMethod.GET)
	public String formCreateUser(User user) {
		return "formUser";
	}
	
	@RequestMapping(value="/updatetUser", method = RequestMethod.POST,consumes="application/json")
	public @ResponseBody void updateUser(@RequestBody User user,HttpSession session) {
		userService.updateUser(user,session);
	}
	
	/*@RequestMapping(value = "/login",method = RequestMethod.GET)
    public String login() {
        return "login";
    }*/
	
	@RequestMapping(value="/getUserByLoginAndPassword", method = RequestMethod.POST,consumes="application/json")
	public @ResponseBody User getUserByLoginAndPassword(@RequestBody User user){
		/*User newUser = new User();
		newUser.setFirstName("NotFound");	
		
		User us = getAllUser().stream().filter(u -> u.getLogin().equals(user.getLogin()) 
				&& u.getPassword().equals(user.getPassword())).findFirst().orElse(newUser);
		System.out.println(us.getLogin());*/
		User use = userServiceImpl.getUserByLoginAndPassword(user.getLogin(), user.getPassword());
		System.out.println(use.getLogin());
		return use;
	} 
	
	@RequestMapping(value="/deleteUser", method = RequestMethod.GET)
	public @ResponseBody void deleteUser(HttpSession session) {
		userService.deleteUser(session);
	}
	
	@RequestMapping(value="/getAllUsers", method = RequestMethod.GET)
	public @ResponseBody Set<User> getAllUser(){
		return userService.getAllUsers();
	}

	@RequestMapping(value="/listUsers", method = RequestMethod.GET)
	public String listUsers(){
		return "listeUsers";
	}
	
	@RequestMapping(value="/getAllDicteeByUser", method = RequestMethod.GET)
	public @ResponseBody List<Dictee> getAllDicteeByUser(HttpSession session){
		User user = (User) session.getAttribute("user");
		long idUser = user.getIdUser();
		return userService.getAllDicteeByUser(idUser);
	}
	
	/*
	 * Cette methode donne les 10 erreurs les plus commises par l'utilisateur par ordre d'echecs.
	 * Le calcule se fait en fonction du type.On fait la diff�rence entre le nombre de fois que l'utilisateur
	 * a �chou� la dict�e d'un type de difficult� et le nombre de fois qu'il a r�ussi le m�me type de difficult�.
	 * 
	 * */
	@RequestMapping(value="/niveau", method = RequestMethod.GET)
	public @ResponseBody List<Profil> countDicteeByUser(HttpSession session){
		Objects.requireNonNull(session);
		User user = (User) session.getAttribute("user");
		long idUser = user.getIdUser();
		//Comme l'insertion se fait en auto increment, on suppose qu'il ne peut y avoir z�ro comme id.
		if(idUser <= 0) throw new IllegalArgumentException("ID user can't negative.");

		List<Profil> list = new ArrayList<>();
		
		Map<String, Long> toutesLesDictees = dicteeService.getAllDictee().stream().map(dicte -> dicte.getSentence()
				.getType().getDescription())
				.collect(Collectors.groupingBy(Function.identity(),Collectors.counting()))
		.entrySet().stream()
		.sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
		.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,(oldValue, newValue) -> oldValue, LinkedHashMap::new));
		
		Map<String, Long> dicteReussies = userService.getAllDicteeByUser(idUser).stream().filter(dicte -> dicte.getResultat() == 1).map(dicte -> dicte.getSentence().getType().getDescription())
				.collect(Collectors.groupingBy(Function.identity(),Collectors.counting()))
				.entrySet().stream()
				.sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,(oldValue, newValue) -> oldValue, LinkedHashMap::new));
		
		Map<String, Long> dicteNonReussies = userService.getAllDicteeByUser(idUser).stream().filter(dicte -> dicte.getResultat() == 0).map(dicte -> dicte.getSentence().getType().getDescription())
			.collect(Collectors.groupingBy(Function.identity(),Collectors.counting()))
			.entrySet().stream()
			.sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
			.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,(oldValue, newValue) -> oldValue, LinkedHashMap::new));
		
		dicteNonReussies.entrySet().forEach(dicteNonReussie ->{
			String key = dicteNonReussie.getKey();
			long value = dicteNonReussie.getValue();
			long nombreTentative = toutesLesDictees.get(key);
			Profil profil = new Profil();
			
			Long dicteReussieEquivalente = dicteReussies.get(key);
			
			if(dicteReussieEquivalente != null) { 
				//niveau.put(key,(value - dicteReussieEquivalente));
				profil.setType(key);
				profil.setNombreTentative(nombreTentative);
				profil.setNombreReussite(dicteReussieEquivalente);
				profil.setNombreEchec(value);
				list.add(profil);
			}else {
				profil.setType(key);
				profil.setNombreTentative(nombreTentative);
				profil.setNombreReussite(0);
				profil.setNombreEchec(value);
				list.add(profil);
			}
		});
		/*niveau.entrySet().stream().sorted(Map.Entry.comparingByValue(Comparator.reverseOrder())).limit(10)
		.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, 
				(oldValue,newValue) -> oldValue, LinkedHashMap::new));*/
		return list;
		
	}
	
	@RequestMapping(value="/detail", method = RequestMethod.GET)
	public String profil(){
		return "detailProfil";
	}
}
