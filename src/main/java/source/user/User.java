package source.user;

import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import source.Auditor;

@Entity
@Table(name="utilisateur")
public class User {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY )
	private long idUser;
	private String firstName;
	private String lastName;
	@Column(unique=true) 
	private String login;
	private String password;
	private String dateInscription;
	private String derniereConnexion;
	private String categorie;
	@Transient
	private final int hashCode = 100000;
	
	public final long getIdUser() {
		return idUser;
	}
	
	public final void setIdUser(long idUser) {
		Auditor.requireNonNegativeValue(idUser);
		this.idUser = idUser;
	}
	
	public final String getLogin() {
		return login;
	}
	
	public final void setLogin(String login) {
		Objects.requireNonNull(login);
		Auditor.isEmpty(login);
		this.login = login;
	}
	
	public final String getPassword() {
		return password;
	}
	
	public final void setPassword(String password) {
		Objects.requireNonNull(password);
		Auditor.isEmpty(password);
		this.password = password;
	}
	
	public final String getFirstName() {
		return firstName;
	}

	public final void setFirstName(String firstName) {
		Objects.requireNonNull(firstName);
		Auditor.isEmpty(firstName);
		this.firstName = firstName;
	}

	public final String getLastName() {
		return lastName;
	}

	public final void setLastName(String lastName) {
		Objects.requireNonNull(lastName);
		Auditor.isEmpty(lastName);
		this.lastName = lastName;
	}

	public final String getDateInscription() {
		return dateInscription;
	}

	public final void setDateInscription(String inscription) {
		Objects.requireNonNull(inscription);
		Auditor.isEmpty(inscription);
		this.dateInscription = inscription;
	}

	public final String getDerniereConnexion() {
		return derniereConnexion;
	}

	public final void setDerniereConnexion(String derniereConnexion) {
		Objects.requireNonNull(derniereConnexion);
		Auditor.isEmpty(derniereConnexion);
		this.derniereConnexion = derniereConnexion;
	}
	
	@Override
	public boolean equals(Object o) {
		if(!(o instanceof User)) return false;
		
		User user = (User) o;
		
		return login.equals(user.login);
	}
	
	@Override
	public int hashCode() {
		return hashCode;
	}

	public final String getCategorie() {
		return categorie;
	}

	public final void setCategorie(String categorie) {
		Objects.requireNonNull(categorie);
		Auditor.isEmpty(categorie);
		this.categorie = categorie;
	}
}
