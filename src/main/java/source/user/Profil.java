package source.user;

import javax.persistence.Transient;

/*
 * Cette classe est cr��e dans le but de correspondre au profil d'un utilisateur et ainsi 
 * avoir des donn�es au format JSON dans la vue.
 * */
public class Profil {

	@Transient
	private String type;
	@Transient
	private long nombreTentative;
	@Transient
	private long nombreReussite;
	@Transient
	private long nombreEchec;
	
	public final String getType() {
		return type;
	}
	public final void setType(String type) {
		this.type = type;
	}
	public final long getNombreTentative() {
		return nombreTentative;
	}
	public final void setNombreTentative(long nombreTentative) {
		this.nombreTentative = nombreTentative;
	}
	public final long getNombreReussite() {
		return nombreReussite;
	}
	public final void setNombreReussite(long nombreReussite) {
		this.nombreReussite = nombreReussite;
	}
	public final long getNombreEchec() {
		return nombreEchec;
	}
	public final void setNombreEchec(long nombreEchec) {
		this.nombreEchec = nombreEchec;
	}
}
