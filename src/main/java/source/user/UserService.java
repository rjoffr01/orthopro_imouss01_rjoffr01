package source.user;

import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpSession;

import source.dictee.Dictee;


public interface UserService {
	
	void createUser(User user);
	void updateUser(User user,HttpSession session);
	void deleteUser(HttpSession session);
	Set<User> getAllUsers();
	User getUserById(long idUser);
	//User getUseByLoginAndPassword(String login,String password);
	List<Dictee> getAllDicteeByUser(long idUser);
}
