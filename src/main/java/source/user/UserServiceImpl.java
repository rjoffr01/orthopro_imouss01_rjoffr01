package source.user;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.apache.commons.collections4.IteratorUtils;
import org.springframework.stereotype.Service;

import source.Auditor;
import source.dictee.Dictee;

@Service
public class UserServiceImpl implements UserService {
	
	@Resource
	private UserRepository userRepository;
	
	@Override
	public void createUser(User user) {
		Objects.requireNonNull(user);
		String dateIns = LocalDateTime.now().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME).toString()
				.replace("T", " ").substring(0, 19);
		user.setDateInscription(dateIns);
		user.setDerniereConnexion(dateIns);
		if(user.getCategorie() == null || user.getCategorie().isEmpty()) {
			user.setCategorie("Ordinaire");
		}
		userRepository.save(user);
	}

	@Override
	public void updateUser(User user,HttpSession session) {
		Objects.requireNonNull(user);
		Objects.requireNonNull(session);
		User use = (User) session.getAttribute("user");
		user.setIdUser(use.getIdUser());
		user.setCategorie(use.getCategorie());
		user.setDateInscription(use.getDateInscription());
		user.setDerniereConnexion(use.getDerniereConnexion());
		userRepository.save(user);
	}

	@Override
	public void deleteUser(HttpSession session) {
		Objects.requireNonNull(session);
		User use = (User) session.getAttribute("user");
		userRepository.delete(use.getIdUser());
	}

	@Override
	public Set<User> getAllUsers() {
		return Collections.unmodifiableSet(new HashSet<>(Collections.unmodifiableList(IteratorUtils.toList(userRepository.findAll().iterator()))));
	}

	@Override
	public User getUserById(long idUser) {
		return userRepository.findOne(idUser);
	}

	@Override
	public List<Dictee> getAllDicteeByUser(long idUser) {
		return userRepository.getDicteeByUser(idUser);
	}


	public User getUserByLoginAndPassword(String login, String password) {
		Objects.requireNonNull(login);
		Objects.requireNonNull(password);
		Auditor.isEmpty(login);
		Auditor.isEmpty(password);
		return userRepository.getUserByLoginAndPassword(login, password);
	}
	
	/*@Override
	public List<Dictee> getAllDicteeByUser(long idUser) {
		User user = getUserById(idUser);
		return user.getListDictee();
	}*/

}
